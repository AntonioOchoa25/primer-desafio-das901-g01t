﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppEstadistica
{
    class Contenido
    {
        private string tipo_contenido = "";
        public Contenido(string tipo_contenido=null)
        {
            this.tipo_contenido = tipo_contenido;
        }

        public String[] Seleccion()
        {
            String[] Arreglo = new String[4];
            switch (tipo_contenido)
            {
                case "Programas de television":
                    Arreglo[0] = $"Un programa de televisión es un conjunto de emisiones periódicas transmitidas por televisión, \n agrupadas bajo un título o cabecera común, en las que a modo de bloque se incluye la mayor , \n parte de los contenidos audiovisuales que se ofrecen en una cadena. En el ámbito , \n profesional, no son considerados programas los bloques de contenidos dedicados a las auto , \n promociones, a la continuidad y a la publicidad convencional. Los programas sirven para , \n entretener, informar, entre otras finalidades.";
                    Arreglo[1] = $"Un último uso es el que se emplea en la propia jerga mediática. Los profesionales del medio \n utilizan este vocablo para describir el contenido que no es informativo, deportivo ni de ficción. \n Según esta acepción, una serie, una película, un partido de fútbol o un noticiario no serían un \n programa, término que quedaría reservado a las restantes líneas de programación \n (concursos, programas de variedades, espacios de telerrealidad, programas nocturnos, etc.), a \n las que también se conoce con el nombre genérico de «variedades». Tal distinción obedece a \n la forma en que la mayoría de las cadenas organizan sus distintas áreas de producción y otras \n fuentes de televisión.";
                    Arreglo[2] = $"::::::::::::  Formatos y géneros de programas  ::::::::::::\n\n - Programa informativo.\n - Programa de telerrealidad.\n - Programa de variedades.\n - Programa de debates.\n - Educativos.\n - Infantiles.\n - Programa de entrevistas.\n - Programa de concursos.\n";
                    Arreglo[3] = $"../png/004-computer.png";
                    break;
                case "Libros de lectura":

                    Arreglo[0] = $":::::::::::::  10 libros ideales para iniciarse en la lectura  ::::::::::::: \n\n 'La casa de los espíritus' de Isabel Allende. \n 'La isla misteriosa' de Julio Verne. \n 'Mujercitas' de Louisa May Alcott. \n 'Harry Potter y la piedra filosofal' de J. \nK Rowling. \n 'Historias de cronopios y de famas' de Julio Cortázar. \n 'Yo, robot' de Isaac Asimov. \n";
                    Arreglo[1] = $"Un libro (del latín liber, libri) es una obra impresa, manuscrita o pintada en una serie de hojas de papel, pergamino, vitela u otro material, unidas por un lado(es decir, encuadernadas) y protegidas con tapas, también llamadas cubiertas. Un libro puede tratar sobre cualquier tema.";
                    Arreglo[2] = $"La lectura es el proceso de comprensión de algún tipo de información o ideas almacenadas en un soporte y transmitidas mediante algún tipo de código, usualmente un lenguaje, que puede ser visual o táctil (por ejemplo, el sistema braille). Otros tipos de lectura pueden no estar basados en el lenguaje tales como la notación o los pictogramas.También se le puede dar el significado como una de las habilidades humanas para descifrar letras o cualquier otro idioma que esté o no inventado por el ser humano.";
                    Arreglo[3] = $"../png/005-notebook.png";
                    break;
                case "Lenguajes de programación":
                    Arreglo[0] = $"Un lenguaje de programación es un lenguaje formal (o artificial, es decir, un lenguaje con reglas gramaticales bien definidas) que le proporciona a una persona, en este caso el programador, la capacidad de escribir (o programar) una serie de instrucciones o secuencias de órdenes en forma de algoritmos con el fin de controlar el comportamiento físico o lógico de un sistema informático, de manera que se puedan obtener diversas clases de datos o ejecutar determinadas tareas. A todo este conjunto de órdenes escritas mediante un lenguaje de programación se le denomina programa informático.";
                    Arreglo[1] = $":::::::::  Lenguajes de programación que existen (y se utilizan) en la actualidad  ::::::::: \n\n - Lenguaje de programación C, C++ y C# \n - Java. \n - Python. \n - PHP. \n - SQL. \n - Ruby. \n - Visual Basic. NET. \n - Lenguaje de programación R. \n";
                    Arreglo[2] = $"El lenguaje de programación es la base para construir todas las aplicaciones digitales que se utilizan en el día a día y se clasifican en dos tipos principales: lenguaje de bajo nivel y de alto nivel.";
                    Arreglo[3] = $"../png/007-website.png";
                    break;

                default:
                    Arreglo[0] = $"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Aliquet risus feugiat in ante metus dictum at. Sit amet commodo nulla facilisi nullam. Eu facilisis sed odio morbi. Dignissim convallis aenean et tortor at risus. Ipsum dolor sit amet consectetur adipiscing elit duis tristique. Leo urna molestie at elementum eu facilisis sed. Condimentum lacinia quis vel eros donec ac odio. Posuere sollicitudin aliquam ultrices sagittis orci a scelerisque purus. Nullam ac tortor vitae purus. Consectetur lorem donec massa sapien faucibus et molestie. Facilisis sed odio morbi quis commodo odio aenean sed adipiscing. Etiam sit amet nisl purus in mollis nunc sed.";
                    Arreglo[1] = $"Nunc sed velit dignissim sodales ut eu. Consectetur adipiscing elit duis tristique sollicitudin nibh sit. Leo urna molestie at elementum eu. Amet nulla facilisi morbi tempus iaculis urna id volutpat lacus. Orci eu lobortis elementum nibh tellus molestie nunc non. Venenatis cras sed felis eget velit aliquet sagittis id consectetur. Fermentum leo vel orci porta. In eu mi bibendum neque egestas congue quisque egestas. Vitae nunc sed velit dignissim sodales ut eu sem. Velit ut tortor pretium viverra suspendisse potenti nullam ac tortor. Consectetur adipiscing elit duis tristique sollicitudin nibh sit. Massa ultricies mi quis hendrerit dolor magna eget est. Enim praesent elementum facilisis leo vel fringilla est ullamcorper eget.";
                    Arreglo[2] = $"Integer eget aliquet nibh praesent tristique magna sit amet purus. Morbi quis commodo odio aenean sed adipiscing diam. Bibendum enim facilisis gravida neque convallis a cras semper auctor. Amet mauris commodo quis imperdiet massa tincidunt. Elit ullamcorper dignissim cras tincidunt. Ligula ullamcorper malesuada proin libero nunc. Ultricies tristique nulla aliquet enim tortor at auctor. Phasellus egestas tellus rutrum tellus pellentesque eu. Fames ac turpis egestas maecenas pharetra. Elit at imperdiet dui accumsan sit amet nulla. Netus et malesuada fames ac. Amet facilisis magna etiam tempor orci eu. Dapibus ultrices in iaculis nunc sed augue lacus. Netus et malesuada fames ac. Pharetra et ultrices neque ornare aenean euismod. Venenatis urna cursus eget nunc scelerisque.";
                    Arreglo[3] = $"../png/001 - idea.png"; 
                    break;
            }
            return Arreglo;
        }
    }
}
