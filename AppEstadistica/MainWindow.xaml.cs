﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AppEstadistica
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        String[] usuarios = new String[5];
        String[] claves = new String[5];
        String[] intereses = new String[5];
        string interes;
        String[] dataIntereses = new String[4];
        string dataInteres;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnIngresar_Click(object sender, RoutedEventArgs e)
        {
            var i = 0;
            bool accesoDenegado = true;
            foreach (var dato in usuarios)
            {
                string clave = pswdClave.Password;
                string usuario = txtUsuario.Text;
                bool usuarioExiste = usuarios.Contains(usuario);
                bool claveExiste = claves.Contains(clave);
                if (usuarioExiste == true && claveExiste == true)
                {
                    accesoDenegado = false;
                    for (int j = 0; j < claves.Length; j++)
                    {
                        if (claves[j] == clave)
                        {
                            dataInteres = intereses[j];
                        }
                    }
                }
                else
                {
                    accesoDenegado = true;
                }
                i++;
            }

            if (accesoDenegado == true)
            {
                MessageBox.Show("La clave o el usuario son incorrectos");
            }
            else
            {
                Contenido c = new Contenido(dataInteres);
                dataIntereses = c.Seleccion();
                Window1 Dashboard = new Window1(dataIntereses);
                Dashboard.Show();
                this.Close();
            }
        }

        private void BtnRegistrarse_Click(object sender, RoutedEventArgs e)
        {
            string clave = pswdClave.Password;
            string usuario = txtUsuario.Text;
            interes = (String)((ComboBoxItem) cbxIntereses.SelectedItem).Content;
            bool usuarioAgregado = false , claveAgregado = false;
            if (usuario.Trim() == null || usuario.Trim() == "")
            {
                MessageBox.Show("El nombre de usuario no puede quedar vacio");
            }
            else
            {
                bool usuarioExiste = usuarios.Contains(usuario);
                if (usuarioExiste == false)
                {
                    if (usuarios.Length < 10)
                    {
                        Array.Resize(ref usuarios, usuarios.Length + 1);
                        usuarios[usuarios.Length - 1] = usuario;
                        usuarioAgregado = true;
                    }
                    else
                    {
                        MessageBox.Show("El registro de usuarios esta lleno");
                    }
                }
            }

            if (clave.Length == 0 || clave.Trim() == "" || clave.Length < 8 || String.IsNullOrEmpty(clave) == true)
            {
                MessageBox.Show("La clave de acceso debe ser de 8 caracteres y no puede quedar vacio");
            }
            else
            {
                bool claveExiste = claves.Contains(clave);
                if (claveExiste == false && usuarioAgregado == true)
                {
                    if (claves.Length < 10)
                    {
                        Array.Resize(ref claves, claves.Length + 1);
                        claves[claves.Length - 1] = clave;
                        claveAgregado = true;

                    }
                    else
                    {
                        MessageBox.Show("El registro de claves de acceso esta lleno");
                    }
                }
            }
            if (interes == "Selecciona una opción")
            {
                MessageBox.Show("La interes de acceso no pueden quedar vacio");
            }
            else
            {
                if (claveAgregado == true)
                {
                    if (intereses.Length < 10)
                    {
                        Array.Resize(ref intereses, intereses.Length + 1);
                        intereses[intereses.Length - 1] = interes;
                    }
                    else
                    {
                        MessageBox.Show("La lista de intereses de acceso esta llena");
                    }
                }

            }
                if (usuarioAgregado == true && claveAgregado == true)
            {
                MessageBox.Show("Datos ingresados con exito");
                usuario = null;
                clave = null;
                pswdClave.Password = "";
                pswdClave.Clear();
                txtUsuario.Text ="";
                txtUsuario.Clear();
            }
            else
            {
                MessageBox.Show("Hubo un problema al agregar las credenciales");
            }

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            usuarios = new String[5] { "ADMIN_1", "ADMIN_2","ADMIN_3","ADMIN_4","ADMIN_5"};
            claves = new String[5] { "ADMIN12345", "ADMIN12346", "ADMIN12347", "ADMIN12348", "ADMIN12349" };
            intereses = new String[5] { "Programas de television", "Lenguajes de programación", "Programas de television", "Libros de lectura", "nada" };
        }

    }
}
