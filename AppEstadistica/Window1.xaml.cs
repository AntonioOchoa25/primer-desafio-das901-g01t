﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AppEstadistica
{
    /// <summary>
    /// Lógica de interacción para Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        String[] datos;
        public Window1(String[] data)
        {
            InitializeComponent();
            this.datos = data;
        }

        private void WindowDashboard_Closed(object sender, EventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
        }

        private void LblContenido_Loaded(object sender, RoutedEventArgs e)
        {
            lblContenido0.Content = this.datos[0];
            lblContenido1.Content = this.datos[1];
            lblContenido2.Content = this.datos[2];
            var MyImage = new BitmapImage(new Uri("@"+this.datos[3], UriKind.Relative));
            imagenIcono.Source = MyImage;
        }
    }
}
